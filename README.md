# Erudilyon

## Présentation

Ce projet est l'application web destinée à l'administrateur/les partenaires de l'application __Erudilyon__. Il permet d'effectuer des opérations basique de modération des différents objets (selon un système de CRUD - Create Read Update Delete).

Cela permet notamment d'offrir une interface en relation avec la base de données.

## API Utilisées

La gestion des logins/mots de passe est gérée par __Microsoft Azure__. Elle permet notamment de vérifier qu'un utilisateur a bien accès à une ressource donnée, permettant de minimiser les risques liés à des utilisations malicieuses de la plateforme web.

L'adresse renseignée par les partenaires dans les objets tels que les activités est gérée par l'API Google Maps. Cette dernière est utilisée via un champ de complétion automatique qui est implémenté directement dans le formulaire d'ajout d'une activité par exemple.

## Installation de l'application web

### `npm install`

Depuis la racine du fichier.

## Lancement de l'application web

### `npm start`

## Application

### Connexion

Dès le lancement de l'application, vous êtes envoyés sur la page de connexion :

![Page de connexion](https://i.imgur.com/NSqSUSj.png)

Cliquer sur le bouton ouvre une fenêtre de connexion Microsoft, sur laquelle on peut se connecter en utilisant un compte Microsoft autorisé. Le cas échéant, il n'est pas possible d'accéder à l'application :

![Authentification Microsoft](https://i.imgur.com/Tq3cHIr.png)

Une fois connecté, l'utilisateur est redirigé vers une page de transition, le temps que les services se réveillent. Cette page est automatiquement redirigée vers l'application dès que possible :

![Page de transition](https://i.imgur.com/w2SuShc.png)

### Utilisation

La navigation est paramétrée selon le type d'utilisateur (administrateur ou partenaire). Dans les deux cas, il suffit d'utiliser le menu de navigation pour parcourir les différentes sections de l'application.