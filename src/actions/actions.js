/**
 * action types
 */

export const GO_TO = 'GO_TO'

/**
 * other constants
 */
export const Navigation = {
INDEX: 'INDEX',
ACTIVITIES: 'ACTIVITIES',
TRIALS: 'TRIALS',
REWARDS: 'REWARDS',
QUESTIONS: 'QUESTIONS',
STATS: 'STATS',
ABOUT: 'ABOUT',
USERS: 'USERS',
}

/**
 * action creators
 */

 export function goTo(url) {
     return { 
        type:GO_TO, 
        url:url
    }
 }