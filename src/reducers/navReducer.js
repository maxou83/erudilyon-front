import { Navigation, GO_TO } from '../actions/actions';

const initialState = {
    Navigation: Navigation.INDEX,
}

//Instanciate default state if state is not defined
const navReducer= (state = initialState, action) => {

    switch (action.type) {
        case GO_TO:
            console.log("Going to : "+ action.url);
            return Object.assign({}, state, {
                Navigation: action.url,
              })
        default:
            return state;
    }
}

export default navReducer;