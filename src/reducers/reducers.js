import { combineReducers } from 'redux';
import navReducer from './navReducer';

const globalReducer = combineReducers({
    navReducer: navReducer
});

export default globalReducer;