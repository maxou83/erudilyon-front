import { connect } from 'react-redux';
import NavHolder from '../components/Navigation/NavHolder';
import { bindActionCreators } from 'redux';
import * as NavigAction from '../actions/actions';


const NavButtonContainer = connect(
	mapStateToProps,
	mapDispatchToProps,
)(NavHolder)