import React from "react";
import ShowInformation from "./components/MainScreen/ShowInformation";
import 'semantic-ui-css/semantic.min.css';
import { Provider } from 'react-redux';
import globalReducer from './reducers/reducers';
import { createStore } from 'redux';
import AuthContent from "./components/authentification/AuthContent";
import Base64 from "base-64";


export const store = createStore(globalReducer,
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());


export default class App extends React.Component {


    constructor(props, context) {
        super(props, context);

        this.state = {
            azureID: undefined,
            user: undefined,
            security: undefined
        };

        this.notifierUtilisateurConnect = this.notifierUtilisateurConnect.bind(this);
    }

    async notifierUtilisateurConnect(azureID) {
        let userJson = undefined;
        let securityJson = undefined;

        await fetch("http://asi-projet-cpe.northeurope.cloudapp.azure.com:8084/credential/" + azureID,
            {
                timeout: 3000,
                crossDomain: true
            })
            .then(res => res.json())
            .then(json => {
                securityJson = json;
                console.log(securityJson)
            })
            .then(() =>

                fetch("http://asi-projet-cpe.northeurope.cloudapp.azure.com:8084/get/" + azureID,
                {
                    timeout: 3000,
                    crossDomain: true,
                    headers: new Headers(
                        {"Authorization": `Basic ${Base64.encode(`${securityJson.user}:${securityJson.pass}`)}`,
                              "Access-Control-Allow-Origin" : "*"}
                    )
                })
                .then(res => res.json())
                .then(json => {
                    userJson = json;
                    console.log(userJson)
                })
            )
            .then(() => {
                    this.setState({
                        azureID: azureID,
                        user: userJson,
                        security: securityJson
                    });
                }
            )
    }


    render() {

        if (this.state.azureID === undefined && this.state.user === undefined && this.state.security === undefined)
            return (<AuthContent notifier={this.notifierUtilisateurConnect}></AuthContent>);
        else
            return (<Provider store={store}><ShowInformation security={this.state.security} user={this.state.user}></ShowInformation></Provider>)
    }

}
