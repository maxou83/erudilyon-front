import React, {Component} from 'react';
import { Menu } from 'semantic-ui-react';
import jsonConfig from '../../configuration/authProperties';
import { UserAgentApplication } from "msal";


const config = {
    auth: jsonConfig
}

class DecoButton extends Component {
    
    handleDeconnexion(){
        console.log("APPAREMMENT TU AS CLIQUE SUR CE BOUTON LOL");
        const userAgentApplication = new UserAgentApplication(config);
        userAgentApplication.logout();
    }
    
    render() {
        return(
                <Menu.Item
                    name={this.props.text}
                    onClick={()=>{this.handleDeconnexion()}}
                />)
    }
}

export default DecoButton;