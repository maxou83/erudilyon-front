import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Menu } from 'semantic-ui-react';
import * as goTo from '../../actions/actions';

// start of code change
const mapStateToProps = (state) => {
    return { Navigation: state.Navigation};
  };


class NavButton extends Component {

    constructor(props){
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }
    
    handleClick(url) {
      this.props.dispatch(goTo.goTo(url));
    }
    
    render() {

        return(
                <Menu.Item
                    name={this.props.text}
                    onClick={()=>{this.handleClick(this.props.url)}}
                />)
    }
}

export default connect(mapStateToProps)(NavButton);