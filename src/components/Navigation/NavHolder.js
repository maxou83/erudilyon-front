import React, {Component} from 'react';
import { Menu } from 'semantic-ui-react';
import NavButton from "./NavButton";
import DecoButton from './DecoButton';

//Contient les boutons/liens de navigation
class NavHolder extends Component {

    render() {
        
        //mode lien, dans la barre de coté
        let renderNav;
        switch(this.props.holderType)
        {
            case "PARTNER":
                renderNav = <Menu vertical>
                <Menu.Item>
                    <NavButton
                        text="Accueil"
                        url="INDEX"
                    />
                    <NavButton
                        text="Activités"
                        url="ACTIVITIES"
                    />
                    <NavButton
                        text="Défi"
                        url="TRIALS"
                    />
                    <NavButton
                        text="Récompenses"
                        url="REWARDS"
                    />
                    <NavButton
                        text="Questions"
                        url="QUESTIONS"
                    />
                    <NavButton
                        text="Stats"
                        url="STATS"
                    />
                    <NavButton
                        text="A Propos"
                        url="ABOUT"
                    />
                    <DecoButton
                        text="Deconnexion"
                    />
                </Menu.Item>
            </Menu>
        break;

        case "ADMIN":
                renderNav = <Menu vertical>
                    <Menu.Item>
                        <NavButton
                            text="Accueil"
                            url="INDEX"
                        />
                        <NavButton
                            text="Activités"
                            url="ACTIVITIES"
                        />
                        <NavButton
                            text="Défi"
                            url="TRIALS"
                        />
                        <NavButton
                            text="Récompenses"
                            url="REWARDS"
                        />
                        <NavButton
                            text="Questions"
                            url="QUESTIONS"
                        />
                        <NavButton
                            text="Stats"
                            url="STATS"
                        />
                        <NavButton
                            text="Utilisateurs"
                            url="USERS"
                        />
                        <DecoButton
                            text="Deconnexion"
                        />
                    </Menu.Item>

                </Menu>
            break;
        default:
            break;
        }
        return renderNav;
        
    }
}


export default NavHolder;
