import React, {Component} from 'react';



class DetailComponent extends Component {

/*
props :
view : read or add
label : what will be display on the ui
typeInput : <input type=typeInput>
nameInput : <input name=nameInput>
handleInputChange : used to notify change to parent (used with onChange event)
value : used only with view=read, value that will fill the input

<DetailComponent
    view=""
    label=""
    typeInput=""
    nameInput=""
    handleInputChange=""
    value="" />

*/
    render() {
        let render_result;
        switch (this.props.view)
        {
            case "add" :
            render_result =
                        <input type={this.props.typeInput} name={this.props.nameInput} onChange={this.props.handleInputChange}></input>


            break;
            case "read" :
                console.log("VALUE : " + this.props.value);
                render_result =
                        <input type={this.props.typeInput} name={this.props.nameInput} value={this.props.value} disabled></input>

            break;
            default :
                console.log("Pas de mode sélectionné pour l'affichage du détailContent");
            break;
         }

        return(
        <div className="field">
            <label>{this.props.label}</label>
            {render_result}
        </div>
        );
    }
}


export default DetailComponent;