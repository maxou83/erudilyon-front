import React, {Component} from 'react';
import * as moment from 'moment';
import Base64 from "base-64";
import DetailComponent from './DetailComponent';
import { connect } from 'react-redux';
import { store } from '../../App';


class Detail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            nomActivity: "",
            dateDebutActivity: "",
            dateFinActivity: "",
            gainActivity: "",
            catActivity: "Cinéma",
            adresseActivity: "",
            imgActivity: "",
            descActivity: "",
            partnerActivity: this.props.user ? this.props.user.userId : "",
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        console.log("I was here : handleInputChange");
        const target = event.target;
        const value = target.type === "date" ? target.value + "T00:00:00.000Z": target.value;
        const name = target.name;

        this.setState({
          [name]: value
        });
    };

    async handleAccept() {
        console.log("Activity accepted, notifying the web service");
        await fetch('http://asi-projet-cpe.northeurope.cloudapp.azure.com:8083/activate',
        {
            method: 'PUT',
            timeout: 3000,
            crossDomain: true,
            headers: new Headers(
                {
                    "Authorization": `Basic ${Base64.encode(`${this.props.security.user}:${this.props.security.pass}`)}`,
                    "Access-Control-Allow-Origin": "*",
                    'Content-Type': 'application/json',
                    'Accept' : 'application/json'
                }
            ),
            body : JSON.stringify(this.props.object)
        })
        .then(res => (res.json()))
        .then(json => {
            this.setState({jsonContent: json});
            console.log("response :")
            console.log(json)
        })
        .catch(error => console.log("just in case " + error));
    }

    async handleReject()
    {
        console.log("Received Reject from modal window");
        //TODO : WIP
        /*if (document.getElementById("rejectReason").value.trim() === '')
            console.log("No reason for rejection received !");
        else*/
            console.log("Activity Rejected, notifying the web service");
            await fetch('http://asi-projet-cpe.northeurope.cloudapp.azure.com:8083/deactivate',
            {
                method: 'PUT',
                timeout: 3000,
                crossDomain: true,
                headers: new Headers(
                    {
                        "Authorization": `Basic ${Base64.encode(`${this.props.security.user}:${this.props.security.pass}`)}`,
                        "Access-Control-Allow-Origin": "*",
                        'Content-Type': 'application/json',
                        'Accept' : 'application/json'
                    }
                ),
                body : JSON.stringify(this.props.object)
            })
            .then(res => (res.json()))
            .then(json => {
                this.setState({jsonContent: json});
                console.log("response :")
                console.log(json)
            })
            .catch(error => console.log("just in case " + error));
    }

    checkRequired()
    {
        return this.state.nomActivity
        && this.state.dateDebutActivity
        && this.state.dateFinActivity
        && this.state.gainActivity
        && this.state.catActivity
        && this.state.adresseActivity
        && this.state.imgActivity
        && this.state.descActivity;
    }
    render() {
        let render_result;
        switch (store.getState().navReducer.Navigation) {
            case 'ACTIVITIES':
                switch (this.props.view)
                {

                    case "add" :
                        render_result =
                            <form className = "ui form" >
                                <DetailComponent
                                    view={this.props.view}
                                    label="Intitulé"
                                    typeInput="text"
                                    nameInput="nomActivity"
                                    handleInputChange={this.handleInputChange}/>

                                <div className="two fields">
                                    <DetailComponent
                                        view={this.props.view}
                                        label="Date de début"
                                        typeInput="date"
                                        nameInput="dateDebutActivity"
                                        handleInputChange={this.handleInputChange}/>

                                    <DetailComponent
                                        view={this.props.view}
                                        label="Date de fin"
                                        typeInput="date"
                                        nameInput="dateFinActivity"
                                        handleInputChange={this.handleInputChange}/>
                                </div>
                                <DetailComponent
                                    view={this.props.view}
                                    label="Récompense"
                                    typeInput="number"
                                    nameInput="gainActivity"
                                    handleInputChange={this.handleInputChange}/>

                                <div className="field">
                                    <label>Catégorie</label>
                                    <select name="catActivity" onChange={this.handleInputChange}>
                                        <option selected >Cinéma</option>
                                        <option>Art</option>
                                        <option>Opéra</option>
                                    </select>
                                </div>

                                <DetailComponent
                                    view={this.props.view}
                                    label="Adresse"
                                    typeInput="text"
                                    nameInput="adresseActivity"
                                    handleInputChange={this.handleInputChange}/>
                                <DetailComponent
                                    view={this.props.view}
                                    label="URL image"
                                    typeInput="text"
                                    nameInput="imgActivity"
                                    handleInputChange={this.handleInputChange} />

                                <div className="field" >
                                    <label>Description</label>
                                    <textarea name="descActivity" onChange={this.handleInputChange}></textarea>
                                </div>
                                <button className="ui primary button" onClick={()=>{this.props.handleAddObject(this.state)}}  disabled={!this.checkRequired()}>
                                    Ajouter
                                </button>
                            </form>;
                    break;
                    case "read" :
                        render_result =
                            <form className = "ui form">
                            <DetailComponent
                                view={this.props.view}
                                label="Intitulé"
                                typeInput="text"
                                nameInput="nomActivity"
                                value={this.props.object.nomActivity}/>

                            <div className="two fields">
                                <DetailComponent
                                    view={this.props.view}
                                    label="Date de début"
                                    typeInput="date"
                                    nameInput="dateDebutActivity"
                                    value={moment(this.props.object.dateDebutActivity).format('YYYY-MM-DD')}/>

                                <DetailComponent
                                    view={this.props.view}
                                    label="Date de fin"
                                    typeInput="date"
                                    nameInput="dateFinActivity"
                                    value={moment(this.props.object.dateFinActivity).format('YYYY-MM-DD')}/>
                            </div>

                            <DetailComponent
                                view={this.props.view}
                                label="Récompense"
                                typeInput="number"
                                nameInput="gainActivity"
                                value={this.props.object.gainActivity}/>

                            <DetailComponent
                                view={this.props.view}
                                label="Catégorie"
                                typeInput="text"
                                nameInput="catActivity"
                                value={this.props.object.catActivity}/>

                            <DetailComponent
                                view={this.props.view}
                                label="Adresse"
                                typeInput="text"
                                nameInput="adresseActivity"
                                value = {this.props.object.adresseActivity}/>


                            <div className="field">
                                <label>Description</label>
                                <textarea value = {this.props.object.descActivity} disabled></textarea>
                            </div>

                            <div className="field">
                                <img className="ui fluid image" src={this.props.object.imgActivity} alt="Illustration"/>
                            </div>

                            </form>;
                    break;
                    default :
                        console.log("Pas de mode sélectionné pour l'affichage du détail");
                    break;
                }
                break;
            case 'REWARDS' :
                switch (this.props.view)
                {
                    case "add" :
                        render_result =
                            <form className = "ui form" >
                                <DetailComponent
                                    view={this.props.view}
                                    label="Intitulé"
                                    typeInput="text"
                                    nameInput="nomActivity"
                                    handleInputChange={this.handleInputChange}/>

                                <div className="two fields">
                                    <DetailComponent
                                        view={this.props.view}
                                        label="Date de début"
                                        typeInput="date"
                                        nameInput="dateDebutActivity"
                                        handleInputChange={this.handleInputChange}/>

                                    <DetailComponent
                                        view={this.props.view}
                                        label="Date de fin"
                                        typeInput="date"
                                        nameInput="dateFinActivity"
                                        handleInputChange={this.handleInputChange}/>
                                </div>
                                <DetailComponent
                                    view={this.props.view}
                                    label="Prix"
                                    typeInput="number"
                                    nameInput="gainActivity"
                                    handleInputChange={this.handleInputChange}/>

                                <DetailComponent
                                    view={this.props.view}
                                    label="Quantité"
                                    typeInput="number"
                                    nameInput="gainActivity"
                                    handleInputChange={this.handleInputChange}/>

                                <DetailComponent
                                    view={this.props.view}
                                    label="URL image"
                                    typeInput="text"
                                    nameInput="imgActivity"
                                    handleInputChange={this.handleInputChange} />

                                <div className="field" >
                                    <label>Description</label>
                                    <textarea name="descActivity" onChange={this.handleInputChange}></textarea>
                                </div>
                                <button className="ui primary button" onClick={()=>{this.props.handleAddObject(this.state)}}  disabled={!this.checkRequired()}>
                                    Ajouter
                                </button>
                            </form>;
                    break;
                    case "read" :
                        console.log(JSON.stringify(this.props.object));
                        render_result =
                            <form className = "ui form">
                            <DetailComponent
                                view={this.props.view}
                                label="Intitulé"
                                typeInput="text"
                                nameInput="nomArticle"
                                value={this.props.object.nomArticle}/>

                            <div className="two fields">
                                <DetailComponent
                                    view={this.props.view}
                                    label="Date de début"
                                    typeInput="date"
                                    nameInput="dateDebutArticle"
                                    value={moment(this.props.object.dateDebutArticle).format('YYYY-MM-DD')}/>

                                <DetailComponent
                                    view={this.props.view}
                                    label="Date de fin"
                                    typeInput="date"
                                    nameInput="dateFinArticle"
                                    value={moment(this.props.object.dateFinArticle).format('YYYY-MM-DD')}/>
                            </div>

                            <DetailComponent
                                view={this.props.view}
                                label="Prix"
                                typeInput="number"
                                nameInput="prixArticle"
                                value={this.props.object.prixArticle}/>

                            <DetailComponent
                                view={this.props.view}
                                label="Quantité"
                                typeInput="number"
                                nameInput="qteArticle"
                                value={this.props.object.qteArticle}/>


                            <div className="field">
                                <label>Description</label>
                                <textarea value = {this.props.object.descArticle} disabled></textarea>
                            </div>

                            <div className="field">
                                <img className="ui fluid image" src={this.props.object.imgArticle} alt="Illustration"/>
                            </div>


                            </form>;
                    break;
                    default :
                        console.log("Pas de mode sélectionné pour l'affichage du détail");
                    break;
                }
            break;
            default :
                render_result =<div></div>;
            break;
        }

        return(
            render_result
        );
    }
}


export default connect()(Detail);