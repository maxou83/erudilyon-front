import React from 'react'
import { Modal, Header, Button } from 'semantic-ui-react'
import Detail from '../Detail/Detail';

class MyModal extends React.Component {

  onBackClick = () => {
    console.log("Détails de l'activité");
    console.log(this.props.object);
    this.props.handleClose();
  }

  handleAccept = () => {
      this.refs.DetailContent.handleAccept();
      this.props.handleClose();
  }

  handleReject = () => {
      this.refs.DetailContent.handleReject();
      this.props.handleClose();
  }


  render() {

    let actions;
    console.log("TYPE D'UTILISATEUR : " + this.props.userType);
    switch(this.props.userType)
    {
      case "ADMIN":
        actions = <Modal.Actions>
            <Button
              type='button'
              icon='checkmark'
              labelPosition='right'
              onClick={this.handleAccept}
              content='Accepter'
            />
            <Button
              type='button'
              icon='times'
              labelPosition='right'
              onClick={this.handleReject}
              content='Refuser'
            />
            <Button
              type='button'
              icon='arrow left'
              labelPosition='right'
              onClick={this.onBackClick}
              content='Retour'
            />
          </Modal.Actions>
        break;
      case "PARTNER":
        actions = <Modal.Actions>
            <Button
              type='button'
              icon='arrow left'
              labelPosition='right'
              onClick={this.onBackClick}
              content='Retour'
            />
          </Modal.Actions>
        break;
      default:
        break;
    }
    

    return (
      <Modal
        open={this.props.modalOpen}
        size='small'
        closeonescape="true"
        closeonrootnodeclick="true"
      >
        <Header icon='browser' content='Détails' />
        <Modal.Content>
          <Detail ref="DetailContent" view="read" object = {this.props.object} objectType = {this.props.objectType} security={this.props.security}/>
        </Modal.Content>
        {actions}
      </Modal>
    )
  }
}


export default MyModal