import React from "react";
import PropTypes from "prop-types";
import AuthProvider from "./AuthProvider";

import "../../App.css";

class AuthContent extends React.Component {
    static propTypes = {
        account: PropTypes.object,
        emailMessages: PropTypes.object,
        error: PropTypes.string,
        graphProfile: PropTypes.object,
        onSignIn: PropTypes.func.isRequired,
        onSignOut: PropTypes.func.isRequired,
        onRequestEmailToken: PropTypes.func.isRequired
    };


    constructor(props, context) {
        super(props, context);
        this.notifierutilisateur = this.notifierutilisateur.bind(this);
    }

    notifierutilisateur(azureID)
    {
        this.props.notifier(azureID);
    }

    render() {
        return (
            <div>
                <section>
                    <h1>
                    Bienvenue sur la page de connexion d'Erudilyon !
                    </h1>
                    {!this.props.account ? (
                        <button onClick={this.props.onSignIn}>Se connecter</button>
                    ) : (
                        <>
                            <button onClick={this.props.onSignOut}>
                                Se déconnecter
                            </button>
                            <button onClick={this.notifierutilisateur(this.props.account.accountIdentifier)}>
                                Accéder à l'application
                            </button>
                        </>
                    )}
                    {this.props.error && (
                        <p className="error">Error: {this.props.error}</p>
                    )}
                </section>
                <section className="data">
                    {this.props.account && (
                        <div className="data-account"><h2>Ceci est une page de transition, l'application arrive...
                        <p> Si vous restez trop longtemps ici, c'est qu'un problème empêche de charger l'application !</p>
                            </h2>
                        </div>
                    )}
                </section>


            </div>
        );
    }
}

export default AuthProvider(AuthContent);