import React, {Component} from 'react';
import Moment from 'react-moment';
import ModaleDetails from '../Modals/ModaleDetails';

class Reward extends Component {

constructor(props) {
        super(props);

        this.state = {
          modalOpen: false,
          valueIntoModal: "123456abcdef"
        }
    }

    render() {
        let visual_render;
        let stateArticle;
        if(this.props.reward.etatArticle)
            stateArticle = "Vérifié";
        else
            stateArticle = "En attente de vérification";
        switch(this.props.view)
        {
            case 'tr' :
                /* bouton supprimer
                    <div className="ui vertical disabled button">
                        Supprimer
                    </div>
                */
                visual_render =(
                        <tr>
                        <td>{this.props.reward.nomArticle}</td>
                        <td>{this.props.reward.prixArticle}</td>
                        <td><Moment parse='YYYY-MM-DDTHH:mm:ss.SSSS+HHmm' format="DD/MM/YYYY">{this.props.reward.dateDebutArticle}</Moment></td>
                        <td><Moment parse='YYYY-MM-DDTHH:mm:ss.SSSS+HHmm' format="DD/MM/YYYY">{this.props.reward.dateFinArticle}</Moment></td>
                        <td>{stateArticle}</td>
                        <td>
                            <div className="ui vertical button" onClick={() => {
                                this.setState({ modalOpen: true })
                            }}>
                                Détails
                            </div>
                            <ModaleDetails // The invisible modal itself
                                key='modal1'
                                modalOpen={this.state.modalOpen}
                                handleClose={() => {this.setState({ modalOpen: false })}}
                                valueIntoModal={this.state.valueIntoModal}
                                object = {this.props.reward}
                                objectType = "Reward"
                                userType = {this.props.userType}/>
                        </td>
                    </tr>)
                    ;
                break;
            default :
                console.log("Pas de mode sélectionné pour l'affichage de la reward");

        }
        return visual_render;
    }
}


export default Reward;