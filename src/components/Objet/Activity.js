import React, {Component} from 'react';
import Moment from 'react-moment';
import ModaleDetails from '../Modals/ModaleDetails';

class Activity extends Component {

    constructor(props) {
        super(props);

        this.state = {
          modalOpen: false,
        }
    }

    render() {
        let visual_render;
        let stateActivity;
        switch(this.props.activity.etatActivity)
        {
            case 0:
                stateActivity = "Refusé";
                break;
            case 1:
                stateActivity = "Vérifié";
                break;
            case 2:
                stateActivity = "En attente de vérification";
                break;
            default:
                break;
        }
        
        switch(this.props.view)
        {
            case 'tr' :
                /* bouton supprimer
                    <div className="ui vertical disabled button">
                        Supprimer
                    </div>
                */
                visual_render =(
                        <tr>
                        <td>{this.props.activity.nomActivity}</td>
                        <td><Moment parse='YYYY-MM-DDTHH:mm:ss.SSSS+HHmm' format="DD/MM/YYYY">{this.props.activity.dateDebutActivity}</Moment></td>
                        <td><Moment parse='YYYY-MM-DDTHH:mm:ss.SSSS+HHmm' format="DD/MM/YYYY">{this.props.activity.dateFinActivity}</Moment></td>
                        <td>{this.props.activity.adresseActivity}</td>
                        <td>{stateActivity}</td>
                        <td>
                            <div className="ui vertical button" onClick={() => {
                                this.setState({ modalOpen: true })
                            }}>
                                Détails
                            </div>
                            <ModaleDetails // The invisible modal itself
                                key='modal1'
                                modalOpen={this.state.modalOpen}
                                handleClose={() => {this.setState({ modalOpen: false })}}
                                object = {this.props.activity}
                                security = {this.props.security}
                                userType = {this.props.userType}/>

                        </td>
                    </tr>)
                    ;
                break;
            default :
                console.log("Pas de mode sélectionné pour l'affichage de l'activity");

        }
        return visual_render;
    }
}


export default Activity;