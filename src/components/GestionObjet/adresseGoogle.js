import React, {Component} from 'react';
import Script from 'react-load-script';
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';
// If you want to use the provided css
import 'react-google-places-autocomplete/dist/assets/index.css';



class ChampAdresseGoogle extends Component {

    render() {
        return(<div>
            <Script url="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_bKO_lR7rpEaKvhRwIr15uBYfdfraLMU&libraries=places"
                />
            <GooglePlacesAutocomplete/>
          </div>)
    }
}
   
export default ChampAdresseGoogle;