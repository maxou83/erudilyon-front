import React, {Component} from 'react';
import Activity from '../Objet/Activity';
import Reward from '../Objet/Reward';
import Base64 from "base-64";
import { store } from "../../App";
import { connect } from "react-redux"


class TableContent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            jsonContent: null,
        };

        this.mountActivity=this.mountActivity.bind(this);
        this.mountRewards=this.mountRewards.bind(this);
    }

    async mountActivity()
    {
    console.log("mountActivity");
        if (this.props.user.type === "ADMIN")
        {
            await fetch('http://asi-projet-cpe.northeurope.cloudapp.azure.com:8083/all',
            {
                method: 'GET',
                timeout: 3000,
                crossDomain: true,
                headers: new Headers(
                    {
                        "Authorization": `Basic ${Base64.encode(`${this.props.security.user}:${this.props.security.pass}`)}`,
                        "Access-Control-Allow-Origin": "*"
                    }
                )
            })
            .then(res => (res.json()))
            .then(json => {
                this.setState({jsonContent: json});
                console.log(json)
            })
            .catch(error => console.log("just in case " + error));
        }
        else
        {
            //I console.log("I was here : " + JSON.stringify(this.props.user));
            await fetch('http://asi-projet-cpe.northeurope.cloudapp.azure.com:8083/allByPartner',
            {
                method: 'POST',
                timeout: 3000,
                crossDomain: true,
                headers: new Headers(
                    {
                        "Authorization": `Basic ${Base64.encode(`${this.props.security.user}:${this.props.security.pass}`)}`,
                        "Access-Control-Allow-Origin": "*",
                        'Content-Type': 'application/json',
                        'Accept' : 'application/json'
                    }
                ),
                body : JSON.stringify(this.props.user)
            })
            .then(res => (res.json()))
            .then(json => {
                this.setState({jsonContent: json});
                console.log(json)
            })
            .catch(error => console.log("just in case " + error));
        }
    }

    async mountRewards()
    {
        console.log("mountRewards");

        if (this.props.user.type === "ADMIN")
        {
            await fetch('http://asi-projet-cpe.northeurope.cloudapp.azure.com:8085/all',
            {
                method: 'GET',
                timeout: 3000,
                crossDomain: true,
                headers: new Headers(
                    {
                        "Authorization": `Basic ${Base64.encode(`${this.props.security.user}:${this.props.security.pass}`)}`,
                        "Access-Control-Allow-Origin": "*"
                    }
                )
            })
            .then(res => (res.json()))
            .then(json => {
                this.setState({jsonContent: json});
                console.log(json)
            })
            .catch(error => console.log("just in case " + error));
        }
        else
        {
            console.log("I was here : " + JSON.stringify(this.props.user));
            await fetch('http://asi-projet-cpe.northeurope.cloudapp.azure.com:8085/articleByPartner',
            {
                method: 'POST',
                timeout: 3000,
                crossDomain: true,
                headers: new Headers(
                    {
                        "Authorization": `Basic ${Base64.encode(`${this.props.security.user}:${this.props.security.pass}`)}`,
                        "Access-Control-Allow-Origin": "*",
                        'Content-Type': 'application/json',
                        'Accept' : 'application/json'
                    }
                ),
                body : "{userId : " + this.props.user.userId + "}"
            })
            .then(res => (res.json()))
            .then(json => {
                this.setState({jsonContent: json});
                console.log(json)
            })
            .catch(error => console.log("just in case " + error));
        }
    }

    componentDidUpdate() {

        if (store.getState().navReducer.Navigation !== this.state.view)
        {
        //console.log("component did mount with user : " + this.props.user.type + " and with state " + store.getState().navReducer.Navigation);
        switch (store.getState().navReducer.Navigation) {
            case 'ACTIVITIES' :
                this.setState({view: store.getState().navReducer.Navigation});
                this.mountActivity();
            break;

            case 'REWARDS' :
                this.setState({view: store.getState().navReducer.Navigation});
                this.mountRewards();
            break;

            default :
                console.log("Erreur tablecontent view : " + store.getState().navReducer.Navigation);
            break;
        }

        }
        /*else
        {
            console.log('no change ' + store.getState().navReducer.Navigation + ' and ' + this.state.view);
        }*/
    }

    render() {
        let display_results_head;
        let display_results;
        switch (store.getState().navReducer.Navigation) {
            case 'INDEX':
                return (<h1>Bonjour, vous êtes connecté en tant que : {this.props.user.displayName}</h1>);
            case 'ACTIVITIES' :
                display_results_head =
                <tr>
                    <th>Intitulé</th>
                    <th>Date de début</th>
                    <th>Date de fin</th>
                    <th>Adresse</th>
                    <th>Etat</th>
                    <th>Actions</th>
                </tr>;

                if (this.state.jsonContent === undefined || this.state.jsonContent === null) {
                    return (<div></div>);
                }
                console.log(this.state.jsonContent);
                display_results = this.state.jsonContent.map(
                (current) =>
                    (<Activity view='tr' key={current.id} activity={current} security={this.props.security} userType={this.props.user.type}> </Activity>)
                );
            break;
            case 'REWARDS' :
                display_results_head =
                <tr>
                    <th>Intitulé</th>
                    <th>Prix</th>
                    <th>Date de début</th>
                    <th>Date de fin</th>
                    <th>Etat</th>
                    <th>Actions</th>
                </tr>;
                if (this.state.jsonContent === undefined || this.state.jsonContent === null) {
                    return (<div></div>);
                }
                display_results = this.state.jsonContent.map(
                (current) =>
                    (<Reward view='tr' key={current.id} reward={current} security={this.props.security} userType={this.props.user.type}> </Reward>)
                );
                break;
            case 'QUESTIONS' :
                display_results_head =
                <tr>
                </tr>;
                break;
            default :
            break;
        };
         return (

             <div>
                 <table className="ui selectable celled table">
                     <thead>
                         {display_results_head}
                     </thead>
                     <tbody>
                         {display_results}
                     </tbody>
                 </table>
             </div>
         );
    }
}

const mapStateToProps = (state, view) => {
    return {
        current_view: state.navReducer
    }
}

export default connect(mapStateToProps)(TableContent);