import React, {Component} from 'react';
import Detail from "../Detail/Detail";
import {Popup } from 'semantic-ui-react';
import Base64 from "base-64";

class ButtonAdd extends Component {

    constructor(props) {
        super(props);

        this.handleAddObject = this.handleAddObject.bind(this);
    }

    async handleAddObject(childState)
    {
        console.log("addobj : ")
        console.log(JSON.stringify(childState));
        await fetch('http://asi-projet-cpe.northeurope.cloudapp.azure.com:8083/add',
        {
            method: 'POST',
            timeout: 3000,
            crossDomain: true,
            headers: new Headers(
                {
                    "Authorization": `Basic ${Base64.encode(`${this.props.security.user}:${this.props.security.pass}`)}`,
                    "Access-Control-Allow-Origin": "*",
                    'Content-Type': 'application/json',
                    'Accept' : 'application/json'
                }
            ),
            body : JSON.stringify(childState)
        })
        .then(res => (res.json()))
        .then(json => {
            this.setState({jsonContent: json});
            console.log("response :" + json)
        })
        .catch(error => console.error("just in case " + error));
    }

    render() {
        return (<Popup trigger={<button className="circular ui icon button">
                                <i className="icon plus"></i>
                        </button>}
                position='right center'
                on = 'click'>
                <Detail view="add" handleAddObject = {this.handleAddObject} user={this.props.user}/>
        </Popup>)

    }
}

export default ButtonAdd;