import React, {Component} from 'react';
import TableContent from "../GestionObjet/TableContent";
import NavHolder from "../Navigation/NavHolder";
import { connect } from 'react-redux';
import ButtonAdd from "../GestionObjet/ButtonAdd";

class ShowInformation extends Component {

    render() {
        let render_result;

        switch(this.props.user.type)
        {
            case "PARTNER":
                render_result =
                    <div className="row">
                        <div className="two wide column"> </div>
                        <ButtonAdd security={this.props.security} user={this.props.user}></ButtonAdd>
                    </div>;
            break;
            case "ADMIN" :
            break;
            case "USER" :
                return(<div></div>);
            default:
            break;
        }
        return(
            <div>
                <div className="ui four column grid">
                    <div className="row"></div>
                    <div className="row">
                        <div className="one wide column">
                        </div>
                        <div className="column">
                            <NavHolder holderType={this.props.user.type}></NavHolder></div>
                        <div >
                            <TableContent security={this.props.security} user={this.props.user} ></TableContent>
                        </div>
                    </div>
                    {render_result}
                </div>
            </div>
        );
    }
}

export default connect()(ShowInformation);